FROM phusion/baseimage

RUN apt-get update \
    && apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common -y
 
RUN apt-get install git ssh sshpass -y

